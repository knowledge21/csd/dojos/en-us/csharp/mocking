﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SalesSystem
{
    public class ComissionCalculator
    {
        public virtual Double calculate(Double p)
        {
            var comission = 0.0;
            if (p >= 0 && p < 10000)
            {
                comission = p * 0.05;
            }
            else
            {
                comission = p * 0.06;
            }

            return Math.Floor(comission * 100)/ 100;
        }
    }
}
