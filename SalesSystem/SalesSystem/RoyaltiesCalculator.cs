﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SalesSystem
{
    public class RoyaltiesCalculator
    {
        private ComissionCalculator comissionCalculator;
        private SalesRepository salesRepository;

        public RoyaltiesCalculator(SalesRepository salesRepository)
        {
            this.comissionCalculator = new ComissionCalculator();
            this.salesRepository = salesRepository;
        }

        public double Calculate(int month, int year)
        {
            double royalty = 0.0;

            List<Sale> sales = salesRepository.GetSales(month, year);
            
            foreach (var sale in sales)
            { 
                var value = sale.value - comissionCalculator.calculate(sale.value);
                royalty += value * 0.2;
            }
            
            return royalty;
        }
    }
}