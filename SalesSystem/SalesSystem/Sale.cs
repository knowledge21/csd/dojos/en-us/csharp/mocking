﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalesSystem
{
    public class Sale
    {
        public Sale(int id, double value)
        {
            this.id = id;
            this.value = value;
        }

        public int id { get; set; }
        public double value { get; set; }
    }
}
