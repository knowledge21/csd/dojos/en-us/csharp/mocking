using Microsoft.VisualStudio.TestTools.UnitTesting;
using SalesSystem;
using Moq;
using System.Collections.Generic;

namespace SalesSystem.Tests
{
    [TestClass]
    public class RoyaltiesCalculatorTests
    {
        [TestMethod]
        public void TestApril2019WithOneSaleOf500Return95USDRoyalties()
        {
            int month = 03;
            int year = 2019;
            double saleValue = 500;
            double expectedRoaylaties = 95;

            var mockedRespository = new Mock<SalesRepository>();
            mockedRespository.Setup(r => r.GetSales(month, year)).Returns(new List<Sale>() { new Sale(1, saleValue) });

            var royaltiesCalculator = new RoyaltiesCalculator(mockedRespository.Object);

            double actualRoyalties = royaltiesCalculator.Calculate(month, year);

            Assert.AreEqual(expectedRoaylaties, actualRoyalties);
        }
    }
}
